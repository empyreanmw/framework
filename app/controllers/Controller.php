<?php


namespace App\controllers;

use App\middleware\ControllerMiddlewareOptions;
use App\ReflectionMethod;

class Controller
{
    protected $middleware;
    protected $parameters;

    public function middleware($middleware, array $options = [])
    {
        foreach ((array) $middleware as $m) {
            $this->middleware[] = [
                'middleware' => $m,
                'options' => &$options,
            ];
        }

        return new ControllerMiddlewareOptions($options);
    }

    public function getMiddleware()
    {
        return $this->middleware;
    }

    public function storeParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    public function callAction($method)
    {
        (new ReflectionMethod($this, $method))->callMethod();
    }

    public function getParameters($key)
    {
        return $this->parameters[$key];
    }
}
