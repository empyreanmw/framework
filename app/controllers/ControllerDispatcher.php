<?php


namespace App\controllers;

use App\Facades\Middleware;

class ControllerDispatcher
{
    public function dispatch($controller, $method, $parameters)
    {
        Middleware::before($controller, $method);

        $controller->storeParameters($parameters);

        $controller->callAction($method);

        Middleware::after();
    }
}
