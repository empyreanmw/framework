<?php

namespace App\controllers;

use App\View;
use App\Models\Project;
use App\Models\Task;
use Request;

class HomeController extends Controller
{
    public function __construct()
    {
        // $this->middleware('Auth');
    }

    public function index()
    {
        return View::get('home');
    }

    public function test(Project $project, Task $task)
    {
        dd([$project, $task]);
    }
}
