<?php

namespace App;

use core\database\Connection;

class App extends Container
{
    protected $serviceProvider;
    protected $debug;
    /**
     * App constructor.
     */
    public function __construct(ServiceProvider $serviceProvider)
    {
        $this->serviceProvider = $serviceProvider;
    }

    public function bootProvider()
    {
        $this->serviceProvider
            ->register()
            ->boot();
    }

    public function run()
    {
        $this->setMode()
             ->makeConnection()
             ->make('router')
             ->direct();
    }

    protected function setMode()
    {
        if (getenv("ENVIRONMENT")=="dev") {
            app()->make("App\Error")->registerHandler()->PrettyPageHandler();
        }

        return $this;
    }

    protected function makeConnection()
    {
        Connection::make();

        return $this;
    }
}
