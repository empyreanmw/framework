<?php


namespace App\shell;

use App\Config;
use App\contracts\ShellScriptInterface;

class PostgressSQLScript implements ShellScriptInterface
{
    public function execute($file)
    {
        shell_exec($this->buildCommand() . ' -f ' . $file);
    }

    protected function buildCommand()
    {
        $database = Config::grab('database')->get('connections.postgress');

        return 'sudo -u '. $database['username']. ' psql ' . $database['name'];
    }
}
