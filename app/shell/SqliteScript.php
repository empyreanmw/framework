<?php


namespace App\shell;

use App\contracts\ShellScriptInterface;
use App\Config;

class SqliteScript implements ShellScriptInterface
{
    public function execute($file)
    {
        shell_exec($this->buildCommand() . ' < ' . $file);
    }

    protected function buildCommand()
    {
        return 'sqlite3 '. Config::grab('database')->get('connections.sqlite');
    }
}
