<?php

namespace App\contracts;

interface BlueprintSyntaxInterface
{
    public function getSyntax();
}
