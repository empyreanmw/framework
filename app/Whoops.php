<?php

namespace App;

use \Whoops\Run;
use \Whoops\Handler\PrettyPageHandler;
use Whoops\Handler\PlainTextHandler;
use Whoops\Handler\JsonResponseHandler;

class Whoops
{
    protected $whoops;
    /**
     * Class constructor.
     */
    public function __construct(Run $run)
    {
        $this->whoops= $run;
    }

    public function PrettyPageHandler()
    {
        $this->whoops->pushHandler(new PrettyPageHandler());

        $this->whoops->register();
    }

    public function PlainTextHandler()
    {
        $this->whoops->pushHandler(new PlainTextHandler());

        $this->whoops->register();
    }

    public function JsonResponseHandler()
    {
        $this->whoops->pushHandler(new JsonResponseHandler());

        $this->whoops->register();
    }

    public function XmlResponseHandler()
    {
        $this->whoops->pushHandler(new XmlResponseHandler());

        $this->whoops->register();
    }
}
