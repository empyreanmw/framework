<?php

use App\contracts\BlueprintSyntaxInterface;

class SqliteBlueprintSyntax implements BlueprintSyntaxInterface
{
    protected $syntax = [
       'autoincrement' => 'AUTOINCREMENT',
       'integer' => "INTEGER"
   ];

    public function getSyntax()
    {
        return $this->syntax;
    }
}
