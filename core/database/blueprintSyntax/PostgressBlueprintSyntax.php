<?php

use App\contracts\BlueprintSyntaxInterface;

class PostgressBlueprintSyntax implements BlueprintSyntaxInterface
{
    protected $syntax = [
       'autoincrement' => 'SERIAL',
       'integer' => ''
   ];

    public function getSyntax()
    {
        return $this->syntax;
    }
}
