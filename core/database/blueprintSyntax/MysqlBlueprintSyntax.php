<?php

use App\contracts\BlueprintSyntaxInterface;

class MysqlBlueprintSyntax implements BlueprintSyntaxInterface
{
    protected $syntax = [
       'autoincrement' => 'AUTO_INCREMENT',
       'integer' => "INTEGER"
   ];

    public function getSyntax()
    {
        return $this->syntax;
    }
}
