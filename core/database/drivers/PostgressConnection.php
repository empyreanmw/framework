<?php

namespace core\database\drivers;

use App\Config;
use App\contracts\ConnectionDriverInterface;
use App\Facades\ShellCommands;
use App\shell\PostgressSQLScript;

class PostgressConnection implements ConnectionDriverInterface
{
    public function connect()
    {
        $database = Config::grab('database')->get('connections.postgress');
        $conn = new \PDO("{$database['host']};port={$database['port']};dbname={$database['name']};user={$database['username']};password={$database['password']}");
        return $conn;
    }

    public function executeSQL($file)
    {
        ShellCommands::execute(new PostgressSQLScript, $file);
    }
}
