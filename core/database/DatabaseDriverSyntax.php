<?php

use App\Config;

class DatabaseDriverSyntax
{
    protected $currentDriver;
    protected $blueprintSyntax = [
        'mysql' => 'MysqlBlueprintSyntax',
        'sqlite' => 'SqliteBlueprintSyntax',
        'postgress' => 'PostgressBlueprintSyntax'
    ];

    public function __construct()
    {
        $this->currentDriver = Config::grab('app')->get('Connection');
    }

    public function getSyntax()
    {
        return app()
                ->make($this->blueprintSyntax[$this->currentDriver])
                ->getSyntax();
    }
}
