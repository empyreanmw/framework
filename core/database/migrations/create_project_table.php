<?php
namespace core\database\migrations;

use core\database\Blueprint;
use App\Facades\SQLBuilder;

class create_project_table
{
    protected $blueprint;
    /**
     * create_user_table constructor.
     */
    public function __construct(Blueprint $blueprint)
    {
        $this->blueprint = $blueprint;
    }

    public function up()
    {
        $this->blueprint->table('projects');
        $this->blueprint->integer('id')->increment()->primary();
        $this->blueprint->string('title');
        $this->blueprint->string('description');

        SQLBuilder::create($this->blueprint);
    }
}
