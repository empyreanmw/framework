<?php
namespace core\database\migrations;

use core\database\Blueprint;
use App\Facades\SQLBuilder;

class create_task_table
{
    protected $blueprint;
    /**
     * create_user_table constructor.
     */
    public function __construct(Blueprint $blueprint)
    {
        $this->blueprint = $blueprint;
    }

    public function up()
    {
        $this->blueprint->table('tasks');
        $this->blueprint->integer('id')->increment()->primary();
        $this->blueprint->string('title');

        SQLBuilder::create($this->blueprint);
    }
}
