<?php

class RouteParameterExtractor
{
    protected $routeParameters = [];
    protected $route;
    protected $newRoute;

    public function extract($route)
    {
        return $this->parseRoute($route)
             ->buildNewRoute();
    }

    protected function parseRoute($route)
    {
        $this->prepareRoute($route);

        return $this;
    }

    protected function buildNewRoute()
    {
        foreach ($this->route as $route) {
            $this->newRoute .= $route."/";
        }
        
        return rtrim($this->newRoute, "/");
    }

    public function getRouteParameters()
    {
        return $this->routeParameters;
    }

    protected function isModel($model)
    {
        return file_exists(url('app/Models/'.ucfirst($model).'.php'));
    }

    protected function extractParameters($value, $key)
    {
        $this->routeParameters[$value] = urldecode($this->route[$key+1]);
    }

    protected function prepareRoute($route)
    {
        $this->route = explode("/", $route);

        foreach ($this->route as $key => $value) {
            if ($this->isModel($value)) {
                $this->extractParameters($value, $key);
                $this->route[$key+1] = '{'.$value.'}';
            }
        }
    }
}
