<?php
return [
   'connections' => [
       'mysql' => [
           'host' => getenv("MYSQL_HOST"),
           'name' => getenv("MYSQL_NAME"),
           'username' => getenv("MYSQL_USERNAME"),
           'password' => getenv("MYSQL_PASSWORD"),
       ],
       'sqlite' => url('storage/sqlite.db'),
       'postgress' => [
           'host' => getenv("POSTGRESS_HOST"),
           'port' => getenv("POSTGRESS_PORT"),
           'name' => getenv("POSTGRESS_NAME"),
           'username' => getenv("POSTGRESS_USERNAME"),
           'password' => getenv("POSTGRESS_PASSWORD")
       ]
   ],
    'drivers' => [
        'mysql' => core\database\drivers\MySQLConnection::class,
        'sqlite' => core\database\drivers\SQLiteConnection::class,
        'postgress' => core\database\drivers\PostgressConnection::class
    ]
];
