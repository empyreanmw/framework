<?php
return [
    'BasePath' => getenv('BASEPATH'),
    'Connection' => getenv('CONNECTION'),
    'Middleware' => [
            'User' => [
                'Test' => App\middleware\TestMiddleware::class,
                'Auth' => App\middleware\AuthMiddleware::class,
                'Guest' => App\middleware\GuestMiddleware::class
            ],
            'Before' => [
                 App\middleware\SetToken::class,
                 App\middleware\VerifyToken::class
                ],
            'After' => [
                App\middleware\SendsValidationErrors::class
            ]
    ],
    'env' => getenv("ENVIRONMENT")
];
