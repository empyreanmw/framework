"use strict";
const ExtractTextWebpackPlugin = require("extract-text-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
  mode: "development",
  entry: {
    app: "./resources/js/app.js"
  },
  output: {
    path: "/home/empyrean/www/framework",
    filename: "./public/js/[name].bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextWebpackPlugin.extract({
          use: "css-loader"
        })
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          "file-loader?name=images/[sha512:hash:base64:7].[ext]",
          {
            loader: "image-webpack-loader",
            options: {
              bypassOnDebug: true, // webpack@1.x
              disable: true // webpack@2.x and newer
            }
          }
        ],
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader"
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
              // options...
            }
          }
        ]
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: "url-loader?limit=100000&name=fonts/[sha512:hash:base64:7.[ext]"
      }
    ]
  },
  plugins: [
    new ExtractTextWebpackPlugin("public/css/mystyles.css"),
    new MiniCssExtractPlugin({
      filename: "public/css/styles.bundle.css"
    })
  ]
};
