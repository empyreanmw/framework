<?php
include __DIR__ . '/header.php';
?>

<body>
    <!--     <div class="flex-center position-ref full-height">

        <div class="top-right links">
            <a href="/home">Home</a>
            <a href="/register">Register</a>
        </div>
        <div class="content">
            <form method="POST" action="login">
                <div class="m-b-md">
                    <label for="username">Username</label>
                    <input type="text" name="username"><br>
                </div><br>
                <div class="m-b-md">
                    <label for="password">Password</label>
                    <input type="password" name="password"><br>
                    <?php
//                echo token();
                ?>
    </div>
    <div class="m-b-md">
        <button type="submit">Login</button>
    </div>
    </form>
    </div>
    </div> -->

    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="https://bulma.io">
                <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
            </a>

            <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false"
                data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="buttons">
                        <a href="/register" class="button is-primary">
                            <strong>Sign up</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <section class="section">
        <div class="container">
            <div class="columns" style="margin-top:250px;">
                <div class="column">
                </div>
                <div class="column is-one-third">
                    <div class="field">
                        <label class="label">Name</label>
                        <div class="control">
                            <input class="input" type="text" placeholder="Text input">
                        </div>
                    </div>

                    <div class="field">
                        <label class="label">Username</label>
                        <div class="control has-icons-left has-icons-right">
                            <input class="input" type="text" placeholder="Text input">
                            <span class="icon is-small is-left">
                                <i class="fas fa-user"></i>
                            </span>
                            <span class="icon is-small is-right">
                                <i class="fas fa-check"></i>
                            </span>
                        </div>
                        <!--                         <p class="help is-success">This username is available</p> -->
                    </div>
                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-link">Submit</button>
                        </div>
                    </div>
                </div>
                <div class="column">
                </div>
            </div>
        </div>
    </section>

</body>